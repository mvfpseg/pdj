plug.dj list:
 pkg.installed:
   - pkgs:
     - nginx
     - sudo
     - openssh-server
     - python
     - python-pip
     - python-flask
     - python-psycopg2
     - iptables

/root/server.py:
  file:
    - managed
    - source: salt://nginx/server.py
    - user: root
    - group: root
    - mode: 644

/etc/nginx/sites-enabled/default:
  file:
    - managed
    - source: salt://nginx/default
    - user: root
    - group: root
    - mode: 644

nginx:
  service.running:
    - enable: True
    - reload: True

bob:
  user:
    - present
    - fullname: Bob
    - shell: /bin/bash
    - home: /home/bob
    - password: '123456'
    - enforce_password: True
    - groups:
       - sudo

/home/bob/.ssh:
  file.directory:
    - user: bob
    - group: bob
    - dir_mode: 755
    - file_mode: 644
    - recurse:
      - user
      - group
      - mode

sshkey:
 cmd.run:
   - name: ssh-keygen -q -N '' -f /home/bob/.ssh/id_rsa
   - runas: bob
   - unless: test -f /home/bob/.ssh/id_rsa

server:
 cmd.run:
   - name: cd /root && python server.py
   - runas: root
   

remoteconnect:
  ssh_auth.present:
    - user: bob
    - require:
      - user: bob
    - source: /home/bob/.ssh/id_rsa.pub
    - config: /%h/.ssh/authorized_keys

access:
 iptables.insert:
    - position: 1
    - in.interface: lo
    - table: filter
    - chain: INPUT
    - dport: 9000
    - proto: TCP
    - jump: ACCEPT
    - save: true

access1:
  iptables.insert:
    - position: 2 
    - table: filter
    - chain: INPUT
    - dport: 9000
    - proto: TCP
    - policy: DROP
    - save: True
