from flask import Flask, jsonify, request
from flask import make_response
from werkzeug.exceptions import Aborter, HTTP_STATUS_CODES
import time
import psycopg2

HTTP_STATUS_CODES[402] = 'Plug.DJ REST Implementation'

app = Flask(__name__)
abort = Aborter()


@app.errorhandler(405 or 500)
def redirect_verb(e):
    message = {'error': 'invalid method'}
    rsp = jsonify(message)
    rsp.status_code = 401
    return rsp


@app.route('/now', methods=['GET'])
def now():
    return jsonify({'time': time.strftime('%Y-%m-%d %H:%M'), 'status': 'ok'})


@app.route('/later', methods=['POST'])
def name():
    if request.json.get('name'):
        data = str(request.json.get('name'))
        if str.isalpha(data):
            return jsonify({'status': 'ok'})
        else:
            abort(make_response(jsonify({'error': 'invalid name', 'payload': data}), 402))

    else:
        abort(405)


@app.route('/check', methods=['GET'])
def check():
    try:
        psycopg2.connect(database="plugdj",
                         user="plugdj",
                         password="plugdj",
                         host="172.17.0.3",
                         port="5432")
        return jsonify({'status': 'ok'})
    except:
        abort(make_response(jsonify({'error': 'cant connect to db'}), 403))


if __name__ == '__main__':
    app.run(debug=False, host='127.0.0.1', port=9000)
