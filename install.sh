apt update && apt install -y python openssh-server wget vim git &&
cd /root &&
wget -O - https://repo.saltstack.com/apt/ubuntu/16.04/amd64/latest/SALTSTACK-GPG-KEY.pub > SALTSTACK_GPG_KEY.pub &&
apt-key add SALTSTACK_GPG_KEY.pub &&
apt update -y &&
apt install -y salt-master salt-minion salt-ssh salt-syndic salt-cloud 
