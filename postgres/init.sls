vm2:
 pkg.installed:
   - pkgs:
     - postgresql
     - postgresql-client
     - sudo
     - openssh-server
     - iptables

obo:
  user:
    - present
    - fullname: Obo
    - shell: /bin/bash
    - home: /home/obo
    - password: '123456'
    - enforce_password: True
    - groups:
       - sudo

sshkey:
 cmd.run:
   - name: ssh-keygen -q -N '' -f /home/oob/.ssh/id_rsa
   - runas: bob
   - unless: test -f /home/obo/.ssh/id_rsa
  
remoteconnect:
  ssh_auth.present:
    - user: obo
    - require:
      - user: obo
    - source: /home/obo/.ssh/id_rsa.pub
    - config: /%h/.ssh/authorized_keys


/etc/postgresql/9.5/main/postgresql.conf:
  file:
    - managed
    - source: salt://postgres/postgresql.conf
    - user: root
    - group: root
    - mode: 644

/etc/postgresql/9.5/main/pg_hba.conf:
  file:
    - managed
    - source: salt://postgres/pg_hba.conf
    - user: root
    - group: root
    - mode: 644

postgresql:
  service.running:
    - enable: True
    - reload: True
     
ph-user:
  postgres_user.present:
    - name: plugdj
    - superuser: True
    - password: 'plugdj'

ph-db:
  postgres_database.present:
    - name: plugdj
    - db_user: plugdj
    - db_password: plugdj
    - db_host: localhost

